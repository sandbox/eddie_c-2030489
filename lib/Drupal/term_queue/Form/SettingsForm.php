<?php

namespace Drupal\term_queue\Form;

use Drupal\system\SystemConfigFormBase;

/**
 * Configures term queue settings for this site.
 */
class SettingsForm extends SystemConfigFormBase {
  /**
   * Implements \Drupal\Core\Form\FormInterface::getFormID().
   */
  public function getFormID() {
    return 'form_term_queue_settings_object';
  }
  
  /**
   * Implements \Drupal\Core\Form\FormInterface::buildForm().
   */
  public function buildForm(array $form, array &$form_state) {
    $form = array();

    $form['global_settings'] = array('#type' => 'fieldset', '#title' => t('Global Settings'));
  
    $form['global_settings']['term_queue_use_autocomplete'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use Autocomplete Textfield When Adding Terms?'),
      '#default_value' => variable_get('term_queue_use_autocomplete', FALSE),
      '#description' => t('Enable this when you have a large number of terms or vocabularies'),
    );
  
    return parent::buildForm($form, $form_state); 
  }
}
